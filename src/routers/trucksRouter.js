const express = require('express');
const {authMiddleware} = require('../middleware/authMiddleware.js');

const router = express.Router();
const {
  getTrucks,
  addUserTruck,
  getTruck,
  updateTruck,
  assignUserTruckById,
  deleteTruck,
} = require('../services/truckService.js');
const { driverMiddleware } = require("../middleware/driverMiddleware");

router.get('/', authMiddleware, driverMiddleware, getTrucks);

router.post('/', authMiddleware, driverMiddleware, addUserTruck);

router.get('/:id', authMiddleware, driverMiddleware, getTruck);

router.put('/:id', authMiddleware, driverMiddleware, updateTruck);

router.post('/:id/assign', authMiddleware, driverMiddleware, assignUserTruckById);

router.delete('/:id', authMiddleware, driverMiddleware, deleteTruck);

module.exports = {
  trucksRouter: router,
};