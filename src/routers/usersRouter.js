const express = require('express');

const router = express.Router();

const { getProfileInfo, deleteProfile, changeProfilePassword } = require('../services/usersService.js');

const {authMiddleware} = require('../middleware/authMiddleware.js');

router.get('/me', authMiddleware, getProfileInfo);

router.delete('/me', authMiddleware, deleteProfile);

router.patch('/me/password', authMiddleware, changeProfilePassword);

module.exports = {
  usersRouter: router,
};
