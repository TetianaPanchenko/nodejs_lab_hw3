const express = require('express');

const router = express.Router();
const {
  getUserLoads,
  addUserLoad,
  getUserActiveLoad,
  iterateLoadState,
  getUserLoadById,
  updateUserLoadById,
  deleteUserLoadById,
  postUserLoadById,
  getUserLoadShippingDetailsById,
} = require('../services/loadsService.js');

const {authMiddleware} = require('../middleware/authMiddleware.js');
const { shipperMiddleware } = require("../middleware/shipperMiddleware");
const { driverMiddleware } = require("../middleware/driverMiddleware");

router.get('/', authMiddleware, getUserLoads);

router.post('/', authMiddleware, shipperMiddleware, addUserLoad);

router.get('/active', authMiddleware, driverMiddleware, getUserActiveLoad);

router.patch('/active/state', authMiddleware, driverMiddleware, iterateLoadState);

router.get('/:id', authMiddleware, getUserLoadById);

router.put('/:id', authMiddleware, shipperMiddleware, updateUserLoadById);

router.delete('/:id', authMiddleware, shipperMiddleware, deleteUserLoadById);

router.post('/:id/post', authMiddleware, shipperMiddleware, postUserLoadById);

router.get('/:id/shipping_info', authMiddleware, shipperMiddleware, getUserLoadShippingDetailsById);

module.exports = {
  loadsRouter: router,
};