const { Load, loadJoiSchema } = require('../models/Load.js');
const { User } = require("../models/Users.js");
const { Truck } = require("../models/Truck.js");
const { loadStates } = require("../utils/loads.utility");
const { findTruck } = require("../utils/truck.utility");

const getUserLoads = async (req, res, next) => {
  try {

    let {offset = 0, limit = 10} = req.query;

    if (limit > 50) {
      res.status(200).json({message: 'Bad request.'})
    }

    const { id: userId, role: userRole } = req.user;

  try{

    if(userRole === 'SHIPPER') {
      const loadsList = await Load.find({ created_by: userId }).skip(offset).limit(limit);
      return res.status(200).json({
        // offset: req.query.offset !== undefined ? +req.query.offset : undefined,
        // limit: req.query.limit !== undefined ? +req.query.limit : undefined,
        offset: req.query.offset,
        limit: req.query.limit,
        count: loadsList.length,
        loads: loadsList,
      });
    }
    Load.find({ assigned_to: userId }).then((loadsList) => {
      if (loadsList.length > 0) {
        res.status(200).json({ loads: loadsList });
      } else {
        res.status(200)
          .json({ message: 'No loads assigned by current DRIVER' });
      }
    });

  } catch (err) {
    return res.status(400).json({
      "message": 'Bad request'
    })
  }
  } catch(error) {
    next(error)
  }
}

const addUserLoad = async (req, res, next) => {
  try {
    const user = await User.findById(req.user.id);

    if (!user) {
      return res.status(400).json({message: 'User profile is not founded'});
    }

    const { name, payload, pickup_address, delivery_address, dimensions } = req.body;
    await loadJoiSchema.validateAsync({ name, payload, pickup_address, delivery_address, dimensions });

    const load = new Load({
      created_by: req.user.id,
      status: 'NEW',
      name,
      payload,
      pickup_address,
      delivery_address,
      dimensions,
      logs: [],
      created_date: new Date().toISOString(),
    });

    await load.save();

    return res.status(200).json({
      message: 'Load created successfully',
    });
  } catch (error) {
    next(error)
  }
}

const getUserActiveLoad = async (req, res, next) => {
  try {
    const user = await User.findById(req.user.id);

    if(!user) {
      return res.status(400).json({message: 'User not found'})
    }

    const {id: userId} = user;

    const load = await Load.findOne({assigned_to: userId});

    if(!load) {
      return res.status(400).json({message: 'No such load' })
    }

    const loadStatus = load.status;

    if(loadStatus !== 'ASSIGNED') {
      return res.status(400).json({message: 'Not active load' })
    }

    return res.status(200).json({load});
  } catch(error) {
    next(error)
  }
}

const iterateLoadState = async (req, res, next) => {
  try {

    const { _id: userId, role } = req.user;

    const assignedTruck = await Truck.findOne({ assigned_to: userId });

    if (!assignedTruck) {
      res.status(400).json({message: 'Assigned truck not found'})
    }

    const activeLoad = await Load.findOne({ assigned_to: userId, status: 'ASSIGNED' });

    if (activeLoad && role === 'DRIVER') {
      const curStateIndex = loadStates.indexOf(activeLoad.state);

      if (curStateIndex >= 0 && curStateIndex < 3) {
        const newState = loadStates[curStateIndex + 1];
        activeLoad.state = newState;
        activeLoad.logs = [...activeLoad.logs, {
          message: `Load state changed to '${newState}'`,
          time: new Date().toISOString(),
        }];

        if (newState === 'Arrived to delivery') {
          activeLoad.status = 'SHIPPED';
          assignedTruck.status = 'IS';
        }

        activeLoad.save();
        assignedTruck.save();

        res.status(200).json({ message: `Load state changed to '${newState}'` });
      } else {
        res.status(200).json({ message: 'Load SHIPPED!' });
      }
    } else {
      res
        .status(400)
        .json({ message: "SHIPPER don't have access to change load state or active load not found!" });
    }

  } catch (error) {
    next(error)
  }
};

const getUserLoadById = async (req, res, next) => {
  try {
    const user = await User.findById(req.user.id);

    if (!user) {
      return res.status(400).json({message: 'User profile is not founded'});
    }

    const {id: loadId} = req.params;
    const {id: userId} = user;

    const load = await Load.findOne({id: loadId, created_by: userId});

    if (load) {
      res.status(200).json({ load });
    } else {
      res
        .status(400)
        .json({ message: "Load not found!" });
    }

    return res.status(200).json({load});

  } catch(error) {
    next(error)
  }
}

const updateUserLoadById = async (req, res, next) => {
  try {
    const user = await User.findById(req.user.id);

    if (!user) {
      return res.status(400).json({message: 'User profile is not founded'});
    }

    const {id: loadId} = req.params;
    const {name, payload, pickup_address, delivery_address, dimensions } = req.body;

    const load = await Load.findOneAndUpdate({_id: loadId, created_by: req.user._id},
      {
        name: name,
        payload: payload,
        pickup_address: pickup_address,
        delivery_address: delivery_address,
        dimensions
    });

    if(!load) {
      return res.status(400)
        .json({message: 'No such load'})
    }

    return res.status(200).json({
      message: 'Load details changed successfully',
    });

  }catch(error) {
    next(error)
  }
}

const deleteUserLoadById = async (req, res, next) => {
  try {
    const user = await User.findById(req.user.id);

    if (!user) {
      return res.status(400).json({message: 'User profile is not founded'});
    }

    const {id: loadId} = req.params;

    const load = await Load.findOneAndDelete({_id: loadId, created_by: req.user._id});

    if(!load) {
      return res.status(400)
        .json({message: 'No such load'})
    }

    return res.status(200).json({
      message: 'Load deleted successfully',
    });

  } catch(error) {
    next(error)
  }
}

const postUserLoadById = async (req, res, next) => {
  try {
    const user = await User.findById(req.user.id);

    if(!user) {
      return res.status(400).json({message: 'User not found'})
    }

    const {id: loadId} = req.params;

    const load = await Load.findById(loadId);

    if (!load) {
      return res.status(400).json({message: 'Load not found'})
    }
    if (load.status !== 'NEW') {
      return res.status(400).json({message: 'Driver can`t be assigned load if status not \'NEW\''})
    }

    await Load.findByIdAndUpdate(loadId, { status: 'POSTED' });
    const assignedFreeTrucks = await Truck.find({
      assigned_to: {$ne: "none"},
      status: {$eq: "IS"},
    });

    const foundTruck = findTruck(load, assignedFreeTrucks);
    if (!foundTruck) {
      await Load.findByIdAndUpdate(loadId, { status: 'NEW' });
      res.json({
        message: "Sorry, we weren't able to find a driver",
        driver_found: false,
      });
      return;
    }

    const { _id: truckId } = foundTruck;
    await Truck.findByIdAndUpdate(truckId, { status: 'OL' });
    const { logs: logsArray } = load;
    logsArray.push({
      message: `Load assigned to driver with id ${foundTruck.created_by}`,
      time: new Date().toLocaleString(),
    });

    await Load.findByIdAndUpdate(loadId, {
      status: 'ASSIGNED',
      state: loadStates[0],
      assigned_to: foundTruck.created_by,
      logs: logsArray,
    });

    return res.status(200).json({
      message: 'Load posted successfully',
      driver_found: true,
    });

  } catch(error) {
    next(error)
  }
}

const getUserLoadShippingDetailsById = async (req, res, next) => {
  try {
    const {id: loadId} = req.params;
    const {id: userId} = req.user;

    const load = await Load.findOne({ created_by: userId, _id: loadId });

    if(!loadId) {
      return res.status(400).json({message: 'Load \'id\' can\'t be found'})
    }

    if (load.assigned_to === 'none') {
      res.status(400).json({ message: 'Load not assigned' });
    } else {
      const truck = await Truck.findOne({ assigned_to: load.assigned_to });
      res.status(200).json({ load, truck });
    }

    } catch(error) {
    next(error)
  }
}

module.exports = {
  getUserLoads,
  addUserLoad,
  getUserActiveLoad,
  iterateLoadState,
  getUserLoadById,
  updateUserLoadById,
  deleteUserLoadById,
  postUserLoadById,
  getUserLoadShippingDetailsById,
};