const bcrypt = require('bcryptjs');

const { User } = require('../models/Users.js');
const mongoose = require("mongoose");
const { createToken } = require("../utils/token.utility");
const Joi = require("joi");

const credentialsJoiSchema = Joi.object({
  email: Joi.string()
    .email({ minDomainSegments: 2 }),

  password: Joi.string()
    .alphanum()
    .required(),
});

const regCredJoiSchema = Joi.object({
  email: Joi.string()
    .email({ minDomainSegments: 2 }),

  password: Joi.string()
    .alphanum()
    .required(),

  role: Joi.string()
    .valid('SHIPPER', 'DRIVER')
    .required()
});

const registerUser = async (req, res, next) => {
  const regCredentials = req.body;

  const joiRes = regCredJoiSchema.validate(regCredentials);

  if(joiRes.error) {
    next({status: 400, message: 'Bad request'})
    return
  }

  const hashedPassword = bcrypt.hashSync(regCredentials.password, 10, (error, hash) => next(error));

  const user = new User({
    email: regCredentials.email,
    password: hashedPassword,
    role: regCredentials.role,
    created_date: new Date().toISOString(),
  });

  await user.save()
    .catch((err) => {
      next(err);
    });

  res.status(200).send({ message: "Success" });
};

const loginUser = async (req, res, next) => {
  try {
    const loginCredentials = req.body;

    const joiRes = credentialsJoiSchema.validate(loginCredentials);

    if(joiRes.error) {
      next({status: 400, message: 'Bad request'})
      return
    }

    const user = await mongoose.model('User').findOne({email: loginCredentials.email});

    if (!user) {
      next({status: 400, message: "Wrong email"});
      return;
    }

    const match = bcrypt.compareSync(loginCredentials.password, user.password, (error) => next(error));

    if (!match) {
      next({status: 400, message: "Wrong password"});
      return;
    }

    const token = await createToken(user._id);

    res.status(200).send({ message: "Success", jwt_token: token });
  } catch (error) {
    next(error)
  }
}

const forgotPassword = async (req, res, next) => {
  try {
    const { email } = req.body;

    if (!email) {
      next({status: 400, message: 'Field \'email\' can\'t be empty'})
      return;
    }

    const user = await mongoose.model('User').findOne({email: email});

    if (!user) {
      next({status: 400, message: "Wrong username"});
      return;
    }

    // Create send password


    res.status(200).send({ message: "New password sent to your email address" });
  }catch(error) {
    next(error)
  }
}

module.exports = {
  registerUser,
  loginUser,
  forgotPassword,
};