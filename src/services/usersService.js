require("dotenv").config();
const bcrypt = require('bcryptjs');
const { User } = require('../models/Users.js');

const getProfileInfo = async (req, res) => {
  const userProfile = await User.findById(req.user._id);

  if (!userProfile) {
    return res.status(400).json({message: 'User profile is not founded'});
  }
  return res.json({
    user: {
      _id: userProfile._id,
      role: userProfile.role,
      email: userProfile.email,
      created_date: userProfile.created_date,
    },
  });
};

const deleteProfile = async (req, res) => {

  const userProfile = await User.findByIdAndDelete(req.user.id);

  if (!userProfile) {
    return res.status(400).json({message: 'User profile is not founded'});
  }

  return res.status(200).json({
    message: "Profile deleted successfully",
  });
};

const changeProfilePassword = async (req, res, next) => {
  try {
    const userProfile = await User.findById(req.user.id);
    const { oldPassword, newPassword } = req.body;

    if (!userProfile) {
      return res.status(400).json({message: 'User not found'});
    }

    const isOldPasswordValid = await bcrypt.compare(
      oldPassword,
      userProfile.password
    );
    if (!isOldPasswordValid) {
      return res.status(400).json({
        message: "Old password is not valid",
      });
    }

    userProfile.password = await bcrypt.hash(
      newPassword,
      parseInt('10')
    );

    return userProfile
      .save()
      .then(() => res.json({ message: "Success" }))
      .catch((err) => next(err));
  } catch(error) {
    next(error)
  }
}

module.exports = {
  getProfileInfo,
  deleteProfile,
  changeProfilePassword
};
