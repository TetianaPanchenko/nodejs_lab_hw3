const { Truck, truckJoiSchema } = require('../models/Truck.js');
const {User} = require("../models/Users.js");
const { getTruckPayload, getLoadDimensions } = require("../utils/truck.utility");

const getTrucks = async (req, res, next) => {
  try {
    const user = await User.findById(req.user.id);
    const {id: userId} = user;

    Truck.find({ created_by: userId }).then((list) => {
      if (list.length > 0) {
        res.status(200).json({ trucks: list });
      } else {
        res.status(200)
          .json({ message: 'No trucks created by current DRIVER' });
      }
    });

  } catch(error) {
    next(error)
  }
}

const addUserTruck = async (req, res, next) => {
  try {
    const user = await User.findById(req.user.id);

    if (!user) {
      return res.status(400).json({message: 'User profile is not founded'});
    }

    const type = req.body.type;
    await truckJoiSchema.validateAsync({ type });

    const truck = new Truck({
      created_by: user.id,
      type,
      payload: getTruckPayload(type),
      dimensions: getLoadDimensions(type),
      created_date: new Date().toISOString(),
    });

    await truck.save();

    return res.status(200).json({
      message: 'Truck created successfully',
    });

  } catch(error) {
    next(error)
  }
}

const getTruck = async (req, res, next) => {
  try {
    const user = await User.findById(req.user.id);

    if (!user) {
      return res.status(400).json({message: 'User profile is not founded'});
    }

    const {id: userId} = user;
    const {id: truckId} = req.params;

    const truck = await Truck.findOne({id: truckId, created_by: userId});

    if(!truck) {
      return res.status(400).json({
        "message": 'No such truck'
      })
    }

    return res.status(200).json({truck});

  } catch(error) {
    next(error)
  }
}

const updateTruck = async (req, res, next) => {
  try {
    const user = await User.findById(req.user.id);

    if (!user) {
      return res.status(400).json({message: 'User profile is not founded'});
    }

    const {id: truckId} = req.params;
    const type = req.body.type;
    await truckJoiSchema.validateAsync({ type });

    const truck = await Truck.findOneAndUpdate({_id: truckId, userId: req.user.id}, {type: type});

    if(!truck) {
      return res.status(400).send("No such truck")
    }

     return res.status(200).json({
       message: 'Truck details changed successfully',
     });

  } catch(error) {
    next(error)
  }
}

const assignUserTruckById = async (req, res, next) => {
  try {
    const user = await User.findById(req.user.id);

    if (!user) {
      return res.status(400).json({message: 'User profile is not founded'});
    }

    const {id: truckId} = req.params;
    const {id: userId} = user;

    const userAssignedTruck = await Truck.find({assigned_to: userId});

    if(userAssignedTruck.length) {
      return res.status(400).json({message: 'User can not assign more than one truck'})
    }

    const truck = await Truck.findOneAndUpdate({_id: truckId, userId: req.user.id}, {assigned_to: userId});

    if(!truck) {
      return res.status(400).send("No such truck")
    }

    res.status(200).json({ message: 'Truck assigned successfully' });

  } catch(error) {
    next(error)
  }
}

const deleteTruck = async (req, res, next) => {
  try {
    const {id: truckId} = req.params;

    const truck = await Truck.findOneAndDelete({_id: truckId, created_by: req.user._id});

    if(!truck) {
      return res.status(400).send("No such truck")
    }

    return res.status(200).json({
      message: 'Truck deleted successfully',
    });

  } catch(error) {
    next(error)
  }
}

module.exports = {
  getTrucks,
  addUserTruck,
  getTruck,
  updateTruck,
  assignUserTruckById,
  deleteTruck,
};