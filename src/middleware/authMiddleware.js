const {User} = require("../models/Users");
const { verifyToken } = require("../utils/token.utility");

const authMiddleware = async (req, res, next) => {
  if (!req.headers.authorization){
    return res.status(400).end();
  }
  if (typeof req.headers.authorization !== 'string'){
    return res.status(400).end();
  }
  let token = req.headers.authorization;
  try {
    let data = await verifyToken(token);
    if (data && data.id){
      const user = await User.findById(data.id)
      if(user) {
        req.user = user;
        next();
      }
      else return res.status(400).end();
    } else {
      res.status(400).end();
    }
  }
  catch(e){
    res.status(400).send(e);
  }
};

module.exports = {
  authMiddleware
};