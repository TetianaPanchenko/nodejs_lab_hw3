const driverMiddleware = (req, res, next) => {
  if(req.user.role !== 'DRIVER') {
    return res.status(400).json({message: 'User profile is not driver'});
  }
  next()
}

module.exports = {
  driverMiddleware
};