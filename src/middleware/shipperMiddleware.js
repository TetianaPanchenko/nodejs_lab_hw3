const shipperMiddleware = (req, res, next) => {
  if(req.user.role !== 'SHIPPER') {
    return res.status(400).json({message: 'User profile is not shipper'});
  }
  next()
}

module.exports = {
  shipperMiddleware
};