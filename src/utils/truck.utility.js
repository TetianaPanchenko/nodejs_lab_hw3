const getTruckPayload = (type) => {
  if (type === 'SPRINTER') {
    return 1700;
  }
  if (type === 'SMALL STRAIGHT') {
    return 2500;
  }
  if (type === 'LARGE STRAIGHT') {
    return 4000;
  }
  return undefined;
};

const getLoadDimensions = (type) => {
  if (type === 'SPRINTER') {
    return {
      width: 300,
      length: 250,
      height: 170,
      payload: getTruckPayload(type),
    };
  }
  if (type === 'SMALL STRAIGHT') {
    return {
      width: 500,
      length: 250,
      height: 170,
      payload: getTruckPayload(type),
    };
  }
  if (type === 'LARGE STRAIGHT') {
    return {
      width: 700,
      length: 350,
      height: 200,
      payload: getTruckPayload(type),
    };
  }
  return undefined;
};

const findTruck = (load, trucks) => {
  trucks.sort((a, b) => b.type.localeCompare(a.type));
  const {
    width: loadWidth,
    length: loadLength,
    height: loadHeight,
  } = load.dimensions;

  for (let i = 0; i < trucks.length; i++) {
    const { type } = trucks[i];

    const truckType = getLoadDimensions(type);
    const { width, length, height, payload } = truckType;

    if (
      width >= loadWidth &&
      length >= loadLength &&
      height >= loadHeight &&
      payload >= load.payload
    ) {
      return trucks[i];
    }
  }
  return null;
};

module.exports = {
  getTruckPayload,
  getLoadDimensions,
  findTruck,
}