const jwt = require("jsonwebtoken");


const verifyToken = async token => {
  // return jwt.verify(token.replace('Bearer ', ''), process.env.SECRET_PASSWORD);
  return jwt.verify(token.replace('JWT Bearer ', ''), process.env.SECRET_PASSWORD);
};

const createToken = async (id) => {
  const token = await jwt.sign({id}, process.env.SECRET_PASSWORD);
  return 'Bearer ' + token;
};


module.exports = {
  verifyToken,
  createToken
};