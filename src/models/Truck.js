const mongoose = require('mongoose');
const Joi = require('joi');

const truckJoiSchema = Joi.object({
  // created_by: Joi.string()
  //   .alphanum()
  //   .required(),
  //
  // assigned_to: Joi.string()
  // .alphanum()
  // .required(),

  type: Joi.string()
    .valid('SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT')
    .required(),

  // status: Joi.string()
  //   .valid('OL', 'IS'),
  //
  // created_date: Joi.string()
  //   .alphanum()
  //   .required(),
});

const truckSchema = new mongoose.Schema({
  created_by: {
    type: String,
    required: true,
  },
  assigned_to: {
    type: String,
    default: 'none',
  },
  type: {
    type: String,
  },
  status: {
    type: String,
    default: 'IS',
    required: true,
  },
  created_date: {
    type: String,
    required: true,
  },
});

const Truck = mongoose.model('Truck', truckSchema);

module.exports = {
  Truck,
  truckJoiSchema,
};