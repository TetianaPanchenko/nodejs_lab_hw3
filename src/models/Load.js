const mongoose = require('mongoose');
const Joi = require('joi');

const loadJoiSchema = Joi.object({
  created_by: Joi.string(),

  assigned_to: Joi.string(),

  status: Joi.string()
    .valid('NEW', 'POSTED', 'ASSIGNED', 'DELIVERED'),

  state: Joi.string()
    .valid('En route to Pick Up', 'Arrived to Pick Up', 'En route to delivery', 'Arrived to delivery'),

  name: Joi.string()
    .required(),

  payload: Joi.number()
    .integer()
    .required(),

  pickup_address: Joi.string()
    .required(),

  delivery_address: Joi.string()
    .required(),

  dimensions: Joi.object()
    .keys({
      width: Joi.number()
        .required(),
      length: Joi.number()
        .required(),
      height: Joi.number()
        .required()
    }),

  logs: Joi.array()
    .items(Joi.object()
      .keys({
        message: Joi.string(),
        time: Joi.date()
      })),
});


const loadSchema = new mongoose.Schema({
  created_by: {
    type: String,
    required: true,
  },
  assigned_to: {
    type: String,
    default: 'none'
  },
  status: {
    type: String,
    enum: ['NEW', 'POSTED', 'ASSIGNED', 'SHIPPED'],
    default: 'NEW',
  },
  state: {
    type: String,
    default: 'none',
  },
  name: {
    type: String,
    required: true,
  },
  payload: {
    type: Number,
    required: true,
  },
  pickup_address: {
    type: String,
    required: true,
  },
  delivery_address: {
    type: String,
    required: true,
  },
  dimensions: {
    type: Object,
    required: true,
  },
  logs: {
    type: Array,
  },
  created_date: {
    type: String,
    required: true,
  },
});

const Load = mongoose.model('Load', loadSchema);

module.exports = {
  Load,
  loadJoiSchema,
};