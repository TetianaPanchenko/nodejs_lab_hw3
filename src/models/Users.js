const mongoose = require('mongoose');
const Joi = require('joi');

const userJoiSchema = Joi.object({
  role: Joi.string()
    .alphanum()
    .required(),

  email: Joi.string()
    .email({ minDomainSegments: 2 }),

  password: Joi.string()
    .alphanum()
    .required(),

  created_date: Joi.string()
    .alphanum()
    .required(),
});

const userSchema = new mongoose.Schema({
  role: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  created_date: {
    type: String,
    required: true,
  },
});

const User = mongoose.model('User', userSchema);

module.exports = {
  User,
  userJoiSchema,
};