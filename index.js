const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');
require('dotenv').config();
const { authRouter } = require('./src/routers/authRouter.js');
const { trucksRouter } = require('./src/routers/trucksRouter.js');
const { loadsRouter } = require('./src/routers/loadsRouter.js');
const { usersRouter } = require('./src/routers/usersRouter.js');

const app = express();

mongoose.connect(process.env.MONGO_CONNECT);
// create database for app ???

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/auth', authRouter);
app.use('/api/trucks', trucksRouter);
app.use('/api/loads', loadsRouter);
app.use('/api/users', usersRouter);

app.get('/', (req, res) => {
  res.send('OK');
})

const start = async () => {
  try {
    app.listen(8080);
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};

start();

// ERROR HANDLER

app.use(errorHandler);

function errorHandler(err, req, res, next) {
  console.error(err);
  if(err.status) {
    res.status(err.status).send({ message: err.message });
    return;
  }
  res.status(500).send({ message: 'Internal server error' });
}
